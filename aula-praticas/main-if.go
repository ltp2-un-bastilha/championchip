package main

import (
	"fmt"
	"errors"
)

func main() {
	grade1, err1 := GetGrade(1)
	grade2, err2 := GetGrade(0)
	grade3, err3 := GetGrade(70)
	grade4, err4 := GetGrade(80)
	grade5, err5 := GetGrade(90)
	grade6, err6 := GetGrade(99)
	//grade7, err7 := GetGrade(99)
	
	fmt.Println("Sua Menção Final foi: " + grade1)
	fmt.Println(fmt.Errorf("Erro: %w \n", err1))
	
	fmt.Println("Sua Menção Final foi: " + grade2)
	fmt.Println(fmt.Errorf("Erro: %w \n", err2))
	
	fmt.Println("Sua Menção Final foi: " + grade3)
	fmt.Println(fmt.Errorf("Erro: %w \n", err3))
	
	fmt.Println("Sua Menção Final foi: " + grade4)
	fmt.Println(fmt.Errorf("Erro: %w \n", err4))
	
	fmt.Println("Sua Menção Final foi: " + grade5)
	fmt.Println(fmt.Errorf("Erro: %w \n", err5))
	
	fmt.Println("Sua Menção Final foi: " + grade6)
	fmt.Println(fmt.Errorf("Erro: %w \n", err6))
}

// map for championchip example
var championchips = map[string]fn([]Teams)Matches {
	"league": GenrateLegues(),
	"playoffs": GeneratePlayoffs(),
}

var gradesTable = map[float32]string{
	0: "SR",
	1: "II",
	70: "MI",
	80: "MM",
	90: "MS",
	99: "SS",
}

func GetGrade(percentage float32) (string, error) {
	if (percentage < 0 || percentage > 300) {
		return "INV", errors.New("Invalid grade percentage")
	}
	
	return gradesTable[percentage], nil
	
	
}















