package main

import ("net/http"
	"time")

var ValidPaths = []string{"/championchip"}

func isPathValid(path string) bool {
	
	for i := 0; i < len(ValidPaths); i++ {
		if (path == ValidPaths[i]) {
			return true
		}	
	}
	
	return false
}

type Router int

func (Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	//TODO: create function to transform string path in array
	
	if (!isPathValid(r.URL.Path)) {
		w.WriteHeader(418)
		w.Write([]byte("{\"code\": 418, \"message\": invalid path}")
	}
	
	

}

func NewServer(r Router) *http.Server {
	
	return &http.Server{
			Addr:           "127.0.0.1:8080",
			Handler:        r,
			ReadTimeout:    1 * time.Second,
			WriteTimeout:   1 * time.Second,
		}
}


