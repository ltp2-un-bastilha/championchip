package main

type Team struct {
	Id string `json:"id"`
	Name string `json:"name"`
}

func (t Team) getName() string {
	return t.Name
}
