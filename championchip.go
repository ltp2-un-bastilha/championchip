package main

import "fmt"

type ChampPlayoff struct {}

func (cp ChampPlayoff) CreateMatches(teams []Team) []Match {
	//TODO: Randomize teams array
	matches := []Match{}
	
	for i := 0; i < len(teams); i = i + 2 {
		m := Match{teamA: teams[i], teamB: teams[i+1]}
		matches = append(matches, m)		
	}
	
	return matches
}

func (cp ChampPlayoff) ShowMatches(matches []Match) {
	for i := 0; i < len(matches); i++ {
		m := matches[i]	
		fmt.Printf("%s x %s \n", m.teamA.name, m.teamB.name)
	} 
}









