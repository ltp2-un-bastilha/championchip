package main

import "fmt"

type Playoff struct {
	Type string `json:"type"` 
	Matches []Match `json:"matches"` 
}

func (cp *Playoff) CreateMatches(teams []Team) {
	var matches []Match
	
	for i := 0; i < len(teams); i = i + 2 {
		m := Match{TeamA: teams[i], TeamB: teams[i+1]}
		matches = append(matches, m)		
	}
	
	cp.Type = "playoff"
	cp.Matches = matches
	
	fmt.Printf("%v", matches)
}










